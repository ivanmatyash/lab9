//
//  FacultiesViewController.m
//  bsuapp
//
//  Created by Admin on 26.05.15.
//  Copyright (c) 2015 dream tp team. All rights reserved.
//

#import "FacultiesViewController.h"
#import "FacultyInfoViewController.h"
#import "AppDelegate.h"

@interface FacultiesViewController ()

@property (strong, nonatomic) UIImageView *headerImageView;
@property (strong, nonatomic) UILabel *headerLabel;
@property (strong, nonatomic) UILabel *usernameLabel;
@property (strong, nonatomic) UIButton *backButton;
@property (strong, nonatomic) UILabel *titleLabel;
@property (strong, nonatomic) NSArray *facultyNames;

@property (strong, nonatomic) UIImageView *bsuImageView;

@property (strong, nonatomic) UITableView *faculties;
@property (strong, nonatomic) NSString *username;

@end

@implementation FacultiesViewController

-(id)initWithUsername: (NSString *) username
{
    self = [super init];
    self.username = username;
    
    self.facultyNames = @[@"radiophysics", @"physical", @"philosophy", @"philological", @"military",
                           @"mechanics_mathematics", @"law", @"international_relations", @"journalism",
                           @"humanitarian", @"historical", @"geographical", @"FAMCS", @"economical",
                           @"chemical", @"biological"];
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)initHeaderWithTitle: (NSString *)title {
    self.headerImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"header.png"]];
    self.headerImageView.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y + 20, self.view.frame.size.width, 70);
    
    self.headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(50, 25, 200, 60)];
    self.headerLabel.text = @"БЕЛОРУССКИЙ\nГОСУДАРСТВЕННЫЙ\nУНИВЕРСИТЕТ";
    self.headerLabel.font = [UIFont fontWithName:@"Helvetica" size:16];
    self.headerLabel.textColor = [UIColor whiteColor];
    self.headerLabel.numberOfLines = 3;
    
    self.usernameLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width - 130, 32, 80, 44)];
    self.usernameLabel.text = self.username;
    self.usernameLabel.textAlignment = NSTextAlignmentCenter;
    
    self.usernameLabel.font = [UIFont fontWithName:@"Helvetica" size:16];
    self.usernameLabel.textColor = [UIColor whiteColor];
    
    UIImage *backImage = [UIImage imageNamed:@"arrow.png"];
    self.backButton = [[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width - 35, 43, 25, 27)];
    [self.backButton setImage:backImage forState:UIControlStateNormal];
    [self.backButton addTarget:self action:@selector(backButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    
    self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(130, 100, 200, 100)];
    self.titleLabel.text = title;
    self.titleLabel.font = [UIFont fontWithName:@"Helvetica Bold" size:21];
    self.titleLabel.textColor = [UIColor colorWithRed:10 / 255.f green:52 / 255.f blue:112 / 255.f alpha:1];
    
    [self.view addSubview: self.headerImageView];
    [self.view addSubview: self.headerLabel];
    [self.view addSubview: self.usernameLabel];
    [self.view addSubview: self.backButton];
    [self.view addSubview: self.titleLabel];
}

- (void)initUI {
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self initHeaderWithTitle: @"Факультеты"];
    
    self.bsuImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo.jpg"]];
    self.bsuImageView.frame = CGRectMake(10, 95, 70, 110);
    
    self.faculties = [[UITableView alloc] initWithFrame: CGRectMake(10, 220, self.view.frame.size.width - 25, self.view.frame.size.height - 230) style:UITableViewStylePlain];
    self.faculties.delegate = self;
    self.faculties.dataSource = self;
    self.faculties.separatorStyle = UITableViewCellSeparatorStyleNone;

    [self.view addSubview: self.bsuImageView];
    [self.view addSubview: self.faculties];
}

- (void)backButtonPressed {
    [self dismissViewControllerAnimated: YES completion: nil];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.facultyNames.count;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSUInteger index = indexPath.section;
    // TODO
    
    AppDelegate *delegate = [UIApplication sharedApplication].delegate;
    NSDictionary *faculty = [delegate getFacultyByName: self.facultyNames[index]];
    
    FacultyInfoViewController *controller = [[FacultyInfoViewController alloc] initWithUsername: self.username andFaculty: faculty];
    [self presentViewController: controller animated:YES completion: nil];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AppDelegate *delegate = [UIApplication sharedApplication].delegate;
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle: UITableViewCellStyleDefault reuseIdentifier: @"cell2"];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    NSMutableArray *names = [[NSMutableArray alloc] init];
    for (NSString *faculty in self.facultyNames) {
        NSDictionary *dict = [delegate getFacultyByName: faculty];
        [names addObject: [dict valueForKey: @"name"]];
    }
    
    NSUInteger index = indexPath.section;
    cell.textLabel.text = names[index];
    
    return cell;
}


@end
