//
//  FacultyInfoViewController.m
//  bsuapp
//
//  Created by Admin on 26.05.15.
//  Copyright (c) 2015 dream tp team. All rights reserved.
//

#import "FacultyInfoViewController.h"
#import "MapViewController.h"

@interface FacultyInfoViewController ()

@property (strong, nonatomic) UILabel *newsLabel;
@property (strong, nonatomic) UIImageView *headerImageView;
@property (strong, nonatomic) UILabel *headerLabel;
@property (strong, nonatomic) UILabel *usernameLabel;
@property (strong, nonatomic) UIScrollView *facultyInfoScrollView;
@property (strong, nonatomic) UITextView *facultyInfoTextView;
@property (strong, nonatomic) UIButton *showOnMapButton;
@property (strong, nonatomic) UIButton *backButton;
@property (strong, nonatomic) NSString *username;
@property (strong, nonatomic) NSDictionary *faculty;

@end

@implementation FacultyInfoViewController

-(id)initWithUsername: (NSString *) username andFaculty: (NSDictionary*)faculty
{
    self = [super init];
    self.faculty = faculty;
    self.username = username;
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initUI];
    // Do any additional setup after loading the view.
}

- (void)initUI
{
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.newsLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 100, self.view.frame.size.width, 100)];
    self.newsLabel.textAlignment = NSTextAlignmentCenter;
    self.newsLabel.text = [self.faculty valueForKey: @"name"];
    self.newsLabel.font = [UIFont fontWithName:@"Helvetica Bold" size:21];
    self.newsLabel.textColor = [UIColor colorWithRed:10 / 255.f green:52 / 255.f blue:112 / 255.f alpha:1];
    
    self.headerImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"header.png"]];
    self.headerImageView.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y + 20, self.view.frame.size.width, 70);
    
    self.headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(50, 25, 200, 60)];
    self.headerLabel.text = @"БЕЛОРУССКИЙ\nГОСУДАРСТВЕННЫЙ\nУНИВЕРСИТЕТ";
    self.headerLabel.font = [UIFont fontWithName:@"Helvetica" size:16];
    self.headerLabel.textColor = [UIColor whiteColor];
    self.headerLabel.numberOfLines = 3;
    
    self.usernameLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width - 130, 32, 80, 44)];
    self.usernameLabel.text = self.username;
    self.usernameLabel.font = [UIFont fontWithName:@"Helvetica" size:16];
    self.usernameLabel.textColor = [UIColor whiteColor];
    self.usernameLabel.textAlignment = NSTextAlignmentCenter;
    
    self.facultyInfoScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(5, 170, self.view.frame.size.width - 20, 420)];
    
    self.facultyInfoTextView = [[UITextView alloc] initWithFrame:CGRectMake(5, 10, self.view.frame.size.width - 20, 420)];
    self.facultyInfoTextView.text = [self.faculty valueForKey: @"description"];
    self.facultyInfoTextView.editable = NO;
    self.facultyInfoTextView.font = [UIFont fontWithName: @"Helvetica" size: 16];
    
    self.showOnMapButton = [[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width / 5, 610, self.view.frame.size.width / 5 * 3, 44)];
    
    [self.showOnMapButton setTitle:@"Показать на карте" forState:UIControlStateNormal];
    self.showOnMapButton.backgroundColor = [UIColor colorWithRed:10 / 255.f green:52 / 255.f blue:112 / 255.f alpha:1];
    self.showOnMapButton.layer.cornerRadius = 5.f;
    [self.showOnMapButton addTarget: self action: @selector(showMapPressed) forControlEvents: UIControlEventTouchUpInside];
    
    [self.facultyInfoScrollView addSubview:self.facultyInfoTextView];
    
    UIImage *backImage = [UIImage imageNamed:@"arrow.png"];
    self.backButton = [[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width - 35, 43, 25, 27)];
    [self.backButton setImage:backImage forState:UIControlStateNormal];
    [self.backButton addTarget:self action:@selector(backButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:self.showOnMapButton];
    [self.view addSubview:self.facultyInfoScrollView];
    [self.view addSubview: self.headerImageView];
    [self.view addSubview: self.headerLabel];
    [self.view addSubview: self.usernameLabel];
    [self.view addSubview:self.newsLabel];
    [self.view addSubview: self.backButton];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)showMapPressed {
    MapViewController *controller = [[MapViewController alloc] initWithUsername: self.username andFaculty: self.faculty];
    [self presentViewController: controller animated:YES completion: nil];
}

- (void) backButtonPressed
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
