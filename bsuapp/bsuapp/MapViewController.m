//
//  MapViewController.m
//  bsuapp
//
//  Created by Admin on 26.05.15.
//  Copyright (c) 2015 dream tp team. All rights reserved.
//

#import <MapKit/MapKit.h>

#import "MapViewController.h"

@interface MapViewController ()

@property (strong, nonatomic) UIImageView *headerImageView;
@property (strong, nonatomic) UILabel *headerLabel;
@property (strong, nonatomic) UILabel *usernameLabel;
@property (strong, nonatomic) UIButton *backButton;
@property (strong, nonatomic) UILabel *titleLabel;

@property (strong, nonatomic) UIImageView *facultyImageView;
@property (strong, nonatomic) MKMapView *mapView;
@property (strong, nonatomic) NSString *username;
@property (strong, nonatomic) NSDictionary *faculty;

@end

@implementation MapViewController

-(id)initWithUsername: (NSString *) username andFaculty:(NSDictionary *)faculty
{
    self = [super init];
    self.faculty = faculty;
    self.username = username;
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)initHeaderWithTitle: (NSString *)title {
    self.headerImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"header.png"]];
    self.headerImageView.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y + 20, self.view.frame.size.width, 70);
    
    self.headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(50, 25, 200, 60)];
    self.headerLabel.text = @"БЕЛОРУССКИЙ\nГОСУДАРСТВЕННЫЙ\nУНИВЕРСИТЕТ";
    self.headerLabel.font = [UIFont fontWithName:@"Helvetica" size:16];
    self.headerLabel.textColor = [UIColor whiteColor];
    self.headerLabel.numberOfLines = 3;
    
    self.usernameLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width - 130, 32, 80, 44)];
    self.usernameLabel.text = self.username;
    self.usernameLabel.textAlignment = NSTextAlignmentCenter;
    
    self.usernameLabel.font = [UIFont fontWithName:@"Helvetica" size:16];
    self.usernameLabel.textColor = [UIColor whiteColor];
    
    UIImage *backImage = [UIImage imageNamed:@"arrow.png"];
    self.backButton = [[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width - 35, 43, 25, 27)];
    [self.backButton setImage:backImage forState:UIControlStateNormal];
    [self.backButton addTarget:self action:@selector(backButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    
    self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 100, self.view.frame.size.width, 100)];
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.titleLabel.text = title;
    self.titleLabel.font = [UIFont fontWithName:@"Helvetica Bold" size:21];
    self.titleLabel.textColor = [UIColor colorWithRed:10 / 255.f green:52 / 255.f blue:112 / 255.f alpha:1];
    
    [self.view addSubview: self.headerImageView];
    [self.view addSubview: self.headerLabel];
    [self.view addSubview: self.usernameLabel];
    [self.view addSubview: self.backButton];
    [self.view addSubview: self.titleLabel];
}

- (void)initUI {
    self.view.backgroundColor = [UIColor whiteColor];
    [self initHeaderWithTitle: [self.faculty valueForKey: @"name"]];
    
    // TODO
    NSString *path = [self.faculty valueForKey: @"photo"];
    self.facultyImageView = [[UIImageView alloc] initWithImage: [UIImage imageNamed: path]];
    self.facultyImageView.frame = CGRectMake(40, 180, self.view.frame.size.width - 80, 150);
    [self.facultyImageView.layer setBorderColor: [UIColor colorWithRed:10 / 255.f green:52 / 255.f blue:112 / 255.f alpha:0.5].CGColor];
    [self.facultyImageView.layer setBorderWidth:2.f];
    
    self.mapView = [[MKMapView alloc] initWithFrame: CGRectMake(20, 345, self.view.frame.size.width - 40, 300)];
    
    CLLocationCoordinate2D coords;
    coords.latitude = [[self.faculty valueForKey: @"latitude"] doubleValue];
    coords.longitude = [[self.faculty valueForKey: @"longitude"] doubleValue];
    MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
    annotation.coordinate = coords;
    [self.mapView addAnnotation: annotation];
    
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(coords, 25000, 25000);
    MKCoordinateRegion adjustedRegion = [self.mapView regionThatFits:viewRegion];
    [self.mapView setRegion:adjustedRegion animated:YES];
    self.mapView.showsUserLocation = YES;
    
    [self.view addSubview: self.facultyImageView];
    [self.view addSubview: self.mapView];
}

- (void)backButtonPressed {
    [self dismissViewControllerAnimated: YES completion: nil];
}

@end
