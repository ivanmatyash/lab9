//
//  User.h
//  bsuapp
//
//  Created by Admin on 02.06.15.
//  Copyright (c) 2015 dream tp team. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface User : NSManagedObject

@property (nonatomic, retain) NSString * login;
@property (nonatomic, retain) NSString * password;
@property (nonatomic, retain) NSString * name;

-(BOOL)isValid;

-(id)initUnbound;

@end
