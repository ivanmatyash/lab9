//
//  ViewController.m
//  bsuapp
//
//  Created by Admin on 25.05.15.
//  Copyright (c) 2015 dream tp team. All rights reserved.
//

#import "ViewController.h"
#import "MainViewController.h"
#import "RegistrationViewController.h"
#import "NewsViewController.h"
#import "AppDelegate.h"
#import "User.h"

@interface ViewController ()

@property (strong, nonatomic) UITextField *loginTextField;
@property (strong, nonatomic) UITextField *passwordTextField;
@property (strong, nonatomic) UIButton *enterButton;
@property (strong, nonatomic) UIButton *registrationButton;
@property (strong, nonatomic) UIImageView *headerImageView;
@property (strong, nonatomic) UILabel *headerLabel;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.view.backgroundColor = [UIColor whiteColor];
    [self initUI];
    
}

- (void)initUI
{
    self.loginTextField = [[UITextField alloc] initWithFrame:CGRectMake(20, -100, 330, 44)];
    self.loginTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Логин" attributes:
    @{NSForegroundColorAttributeName: [UIColor colorWithRed:1 green:1 blue:1 alpha:0.5]}];
    self.loginTextField.textColor = [UIColor whiteColor];
    self.loginTextField.textAlignment = NSTextAlignmentCenter;
    self.loginTextField.backgroundColor = [UIColor colorWithRed:10 / 255.f green:52 / 255.f blue:112 / 255.f alpha:0.45];
    
    __weak ViewController *weakSelf = self;
    [UIView animateWithDuration:1.5f animations:^{
        weakSelf.loginTextField.frame = CGRectMake(weakSelf.loginTextField.frame.origin.x, weakSelf.view.frame.size.height / 2 - 60, weakSelf.loginTextField.frame.size.width, weakSelf.loginTextField.frame.size.height);
    }];
    
    self.passwordTextField = [[UITextField alloc] initWithFrame:CGRectMake(20, -100, 330, 44)];
    self.passwordTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Пароль" attributes:
                                                 @{NSForegroundColorAttributeName: [UIColor colorWithRed:1 green:1 blue:1 alpha:0.5]}];
    self.passwordTextField.textColor = [UIColor whiteColor];
    self.passwordTextField.textAlignment = NSTextAlignmentCenter;
    self.passwordTextField.backgroundColor = [UIColor colorWithRed:10 / 255.f green:52 / 255.f blue:112 / 255.f alpha:0.45];
    
    [UIView animateWithDuration:1.5f animations:^{
        weakSelf.passwordTextField.frame = CGRectMake(weakSelf.passwordTextField.frame.origin.x, weakSelf.view.frame.size.height / 2, weakSelf.passwordTextField.frame.size.width, weakSelf.passwordTextField.frame.size.height);
    }];
    
    self.enterButton = [[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width / 3.f, 1000, self.view.frame.size.width / 3, 44)];
    self.enterButton.backgroundColor = [UIColor colorWithRed:10 / 255.f green:52 / 255.f blue:112 / 255.f alpha:1];
    [self.enterButton setTitle:@"Вход" forState:UIControlStateNormal];
    [self.enterButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.enterButton.layer.cornerRadius = 5.f;
    
    [UIView animateWithDuration:1.5f animations:^{
        weakSelf.enterButton.frame = CGRectMake(weakSelf.enterButton.frame.origin.x, weakSelf.view.frame.size.height / 2 + 100, weakSelf.enterButton.frame.size.width, weakSelf.enterButton.frame.size.height);
    }];
    
    self.registrationButton = [[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width / 3.f, 1000, self.view.frame.size.width / 3, 44)];
    self.registrationButton.backgroundColor = [UIColor colorWithRed:10 / 255.f green:52 / 255.f blue:112 / 255.f alpha:1];
    [self.registrationButton setTitle:@"Регистрация" forState:UIControlStateNormal];
    [self.registrationButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.registrationButton.layer.cornerRadius = 5.f;
    
    [UIView animateWithDuration:1.5f animations:^{
        weakSelf.registrationButton.frame = CGRectMake(weakSelf.registrationButton.frame.origin.x, weakSelf.view.frame.size.height / 2 + 150, weakSelf.registrationButton.frame.size.width, weakSelf.registrationButton.frame.size.height);
    }];
    
    self.headerImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"header.png"]];
    self.headerImageView.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y + 20, self.view.frame.size.width, 70);
    
    self.headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(50, 25, 200, 60)];
    self.headerLabel.text = @"БЕЛОРУССКИЙ\nГОСУДАРСТВЕННЫЙ\nУНИВЕРСИТЕТ";
    self.headerLabel.font = [UIFont fontWithName:@"Helvetica" size:16];
    self.headerLabel.textColor = [UIColor whiteColor];
    self.headerLabel.numberOfLines = 3;
    [UIView animateWithDuration:1.5f animations:^{
        weakSelf.headerImageView.alpha = 0;
        weakSelf.headerImageView.alpha = 1;
        weakSelf.headerLabel.alpha = 0;
        weakSelf.headerLabel.alpha = 1;
    }];
    
    [self.enterButton addTarget:self action:@selector(loginPressed) forControlEvents:UIControlEventTouchUpInside];
    [self.registrationButton addTarget:self action:@selector(registerPressed) forControlEvents: UIControlEventTouchUpInside];
    
    UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closeKeyBoard)];
    
    [self.view addGestureRecognizer:recognizer];
    
    [self.view addSubview:self.headerLabel];
    [self.view addSubview:self.headerImageView];
    [self.view addSubview:self.registrationButton];
    [self.view addSubview:self.enterButton];
    [self.view addSubview:self.loginTextField];
    [self.view addSubview:self.passwordTextField];
    [self.view bringSubviewToFront:self.headerLabel];
}

- (void)closeKeyBoard
{
    for(UIView *view in self.view.subviews)
    {
        [view resignFirstResponder];
    }
}

- (void)loginPressed {
    // TODO
    AppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [delegate managedObjectContext];
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"User" inManagedObjectContext: context];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity: entityDescription];
    
    NSPredicate *pred = [NSPredicate predicateWithFormat: @"login == %@ AND password == %@", self.loginTextField.text, self.passwordTextField.text];
    [request setPredicate: pred];
    
    NSError *err;
    NSArray *records = [context executeFetchRequest: request error: &err];
    
    
    if (records.count == 0) {
        NSString *msg = @"Неверный логин или пароль";
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"Fail!" message: msg delegate: nil cancelButtonTitle: @"OK" otherButtonTitles: nil];
        [alert show];
        return;
    }
    
    User *record = records[0];
    
    MainViewController *mainController = [[MainViewController alloc] initWithUsername:record.name];
    [self presentViewController: mainController animated:YES completion:nil];
    self.loginTextField.text = @"";
    self.passwordTextField.text = @"";
}

- (IBAction)registerPressed {
    RegistrationViewController *registrationController = [[RegistrationViewController alloc] init];
    [self presentViewController: registrationController animated: YES completion: nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
