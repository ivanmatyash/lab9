//
//  NewsViewController.h
//  bsuapp
//
//  Created by Admin on 26.05.15.
//  Copyright (c) 2015 dream tp team. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsViewController : UIViewController

-(id)initWithUsername: (NSString *) username;

@end
