//
//  RegistrationViewController.m
//  bsuapp
//
//  Created by Admin on 25.05.15.
//  Copyright (c) 2015 dream tp team. All rights reserved.
//

#import <CoreData/CoreData.h>

#import "User.h"
#import "AppDelegate.h"
#import "RegistrationViewController.h"

@interface RegistrationViewController ()

@property (strong, nonatomic) AppDelegate *delegate;

@property (strong, nonatomic) UIImageView *headerImageView;
@property (strong, nonatomic) UILabel *headerLabel;

@property (strong, nonatomic) UITextField *name;
@property (strong, nonatomic) UITextField *login;
@property (strong, nonatomic) UITextField *password;

@property (strong, nonatomic) UIButton *registerButton;
@property (strong, nonatomic) UIButton *backButton;

@end

@implementation RegistrationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self initUI];
}

- (void)initUI {
    self.delegate = [UIApplication sharedApplication].delegate;
    __weak RegistrationViewController *weakSelf = self;
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.headerImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"header.png"]];
    self.headerImageView.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y + 20, self.view.frame.size.width, 70);
    
    self.headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(50, 25, 200, 60)];
    self.headerLabel.text = @"БЕЛОРУССКИЙ\nГОСУДАРСТВЕННЫЙ\nУНИВЕРСИТЕТ";
    self.headerLabel.font = [UIFont fontWithName:@"Helvetica" size:16];
    self.headerLabel.textColor = [UIColor whiteColor];
    self.headerLabel.numberOfLines = 3;
    [UIView animateWithDuration:1.5f animations:^{
        weakSelf.headerImageView.alpha = 0;
        weakSelf.headerImageView.alpha = 1;
        weakSelf.headerLabel.alpha = 0;
        weakSelf.headerLabel.alpha = 1;
    }];
    
    self.name = [[UITextField alloc] initWithFrame: CGRectMake(
        20, self.view.frame.size.height / 2 - 180, self.view.frame.size.width - 40, 44)];
    self.name.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Имя пользователя" attributes: @{NSForegroundColorAttributeName: [UIColor colorWithRed:1 green:1 blue:1 alpha:0.5]}];
    self.name.textColor = [UIColor whiteColor];
    self.name.textAlignment = NSTextAlignmentCenter;
    self.name.backgroundColor = [UIColor colorWithRed:10 / 255.f green:52 / 255.f blue:112 / 255.f alpha:0.45];
    
    self.login = [[UITextField alloc] initWithFrame: CGRectMake(
        20, self.view.frame.size.height / 2 - 120, self.view.frame.size.width - 40, 44)];
    self.login.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Логин" attributes: @{NSForegroundColorAttributeName: [UIColor colorWithRed:1 green:1 blue:1 alpha:0.5]}];
    self.login.textColor = [UIColor whiteColor];
    self.login.textAlignment = NSTextAlignmentCenter;
    self.login.backgroundColor = [UIColor colorWithRed:10 / 255.f green:52 / 255.f blue:112 / 255.f alpha:0.45];
    
    self.password = [[UITextField alloc] initWithFrame: CGRectMake(
        20, self.view.frame.size.height / 2 - 60, self.view.frame.size.width - 40, 44)];
    self.password.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Пароль" attributes: @{NSForegroundColorAttributeName: [UIColor colorWithRed:1 green:1 blue:1 alpha:0.5]}];
    self.password.textColor = [UIColor whiteColor];
    self.password.textAlignment = NSTextAlignmentCenter;
    self.password.backgroundColor = [UIColor colorWithRed:10 / 255.f green:52 / 255.f blue:112 / 255.f alpha:0.45];
    
    self.registerButton = [[UIButton alloc] initWithFrame:CGRectMake(
        self.view.frame.size.width / 3.f, self.view.frame.size.height / 2 + 60, self.view.frame.size.width / 3.f, 44)];
    self.registerButton.backgroundColor = [UIColor colorWithRed:10 / 255.f green:52 / 255.f blue:112 / 255.f alpha:1];
    [self.registerButton setTitle:@"Регистрация" forState:UIControlStateNormal];
    [self.registerButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.registerButton.layer.cornerRadius = 5.f;
    
    UIImage *backImage = [UIImage imageNamed:@"arrow.png"];
    self.backButton = [[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width - 35, 43, 25, 27)];
    [self.backButton setImage:backImage forState:UIControlStateNormal];
    
    [self.registerButton addTarget:self action:@selector(registerPressed) forControlEvents:UIControlEventTouchUpInside];
    [self.backButton addTarget:self action:@selector(backPressed) forControlEvents:UIControlEventTouchUpInside];
    
    UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closeKeyBoard)];
    
    [self.view addGestureRecognizer:recognizer];
    
    [self.view addSubview: self.headerImageView];
    [self.view addSubview: self.headerLabel];
    
    [self.view addSubview: self.name];
    [self.view addSubview: self.login];
    [self.view addSubview: self.password];
    
    [self.view addSubview: self.registerButton];
    [self.view addSubview: self.backButton];
}

- (void)closeKeyBoard
{
    for(UIView *view in self.view.subviews)
    {
        [view resignFirstResponder];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSArray*)getAllUsers
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"User"
                                   inManagedObjectContext:self.delegate.managedObjectContext];
    [fetchRequest setEntity:entity];
    NSError* error;
    NSArray *fetchedRecords = [self.delegate.managedObjectContext
                               executeFetchRequest:fetchRequest error:&error];
    return fetchedRecords;
}

- (void)registerPressed {
    User *user = [NSEntityDescription insertNewObjectForEntityForName: @"User" inManagedObjectContext:self.delegate.managedObjectContext];
    user.login = self.login.text;
    user.password = self.password.text;
    user.name = self.name.text;
    
    NSString *msg;
    
    if ([user isValid]) {
        [self.delegate saveContext];
        msg = @"Вы успешны";
    } else {
        msg = @"Ваша жизнь полна неудач";
    }
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"You are beautiful" message: msg delegate: nil cancelButtonTitle: @"OK" otherButtonTitles: nil];
    [alert show];
    
    /*NSArray *users = [self getAllUsers];
    for (User *user in users) {
        NSLog(@"%@", user.login);
    }*/
    
    if ([user isValid]) {
        [self dismissViewControllerAnimated: YES completion: nil];
    }
}

- (void)backPressed {
    [self dismissViewControllerAnimated: YES completion: nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
