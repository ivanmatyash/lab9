//
//  User.m
//  bsuapp
//
//  Created by Admin on 02.06.15.
//  Copyright (c) 2015 dream tp team. All rights reserved.
//

#import "User.h"
#import "AppDelegate.h"


@implementation User

@dynamic login;
@dynamic password;
@dynamic name;

-(BOOL)isValid {
    return ![self.login isEqualToString: @""] && ![self.password isEqualToString: @""] && ![self.name isEqualToString: @""] && self.name.length < 9;
}

-(id)initUnbound {
    AppDelegate *delegate = [UIApplication sharedApplication].delegate;
    NSEntityDescription *entity = [NSEntityDescription entityForName: @"User" inManagedObjectContext: delegate.managedObjectContext];
    return [self initWithEntity: entity insertIntoManagedObjectContext: nil];
}

@end
