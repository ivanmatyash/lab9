//
//  MainViewController.m
//  bsuapp
//
//  Created by Admin on 25.05.15.
//  Copyright (c) 2015 dream tp team. All rights reserved.
//

#import "MainViewController.h"
#import "NewsViewController.h"
#import "FacultiesViewController.h"

@interface MainViewController ()

@property (strong, nonatomic) UIImageView *headerImageView;
@property (strong, nonatomic) UILabel *headerLabel;
@property (strong, nonatomic) UILabel *usernameLabel;

@property (strong, nonatomic) UILabel *infoLabel;

@property (strong, nonatomic) UITableView *stuff;

@property (strong, nonatomic) UILabel *newsLabel;
@property (strong, nonatomic) UILabel *facultiesLabel;
@property (strong, nonatomic) UIButton *logoutButton;
@property (strong, nonatomic) NSString *username;

@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initUI];
}

-(id)initWithUsername: (NSString *) username
{
    self = [super init];
    self.username = username;
    return self;
}

- (void)initUI {
    //__weak MainViewController *weakSelf = self;
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.headerImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"header.png"]];
    self.headerImageView.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y + 20, self.view.frame.size.width, 70);
    
    self.headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(50, 25, 200, 60)];
    self.headerLabel.text = @"БЕЛОРУССКИЙ\nГОСУДАРСТВЕННЫЙ\nУНИВЕРСИТЕТ";
    self.headerLabel.font = [UIFont fontWithName:@"Helvetica" size:16];
    self.headerLabel.textColor = [UIColor whiteColor];
    self.headerLabel.numberOfLines = 3;
    
    self.usernameLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width - 130, 32, 80, 44)];
    self.usernameLabel.text = self.username;
    self.usernameLabel.font = [UIFont fontWithName:@"Helvetica" size:16];
    self.usernameLabel.textColor = [UIColor whiteColor];
    self.usernameLabel.textAlignment = NSTextAlignmentCenter;
    
    self.infoLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 70, self.view.frame.size.width - 40, 200)];
    self.infoLabel.numberOfLines = 6;
    self.infoLabel.text = @"Контактная информация:\n\nАдрес: пр. Независимости, 4,\n220030, г. Минск, Республика\nБеларусь\nТел. +375 17 209 50 44";
    self.infoLabel.textColor = [UIColor colorWithRed:10 / 255.f green:52 / 255.f blue:112 / 255.f alpha:1];
    
    self.stuff = [[UITableView alloc] initWithFrame: CGRectMake(10, 300, self.view.frame.size.width - 25, self.view.frame.size.height - 310) style:UITableViewStylePlain];
    self.stuff.delegate = self;
    self.stuff.dataSource = self;
    self.stuff.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    self.newsLabel = [[UILabel alloc] initWithFrame: CGRectMake(10, 20, 100, 20)];
    self.newsLabel.textColor = [UIColor colorWithRed:10 / 255.f green:52 / 255.f blue:112 / 255.f alpha:1];
    self.newsLabel.text = @"Новости";
    
    self.facultiesLabel = [[UILabel alloc] initWithFrame: CGRectMake(10, 20, 100, 20)];
    self.facultiesLabel.textColor = [UIColor colorWithRed:10 / 255.f green:52 / 255.f blue:112 / 255.f alpha:1];
    self.facultiesLabel.text = @"Факультеты";
    
    UIImage *backImage = [UIImage imageNamed:@"door.png"];
    self.logoutButton = [[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width - 40, 33, 35, 45)];
    [self.logoutButton setImage:backImage forState:UIControlStateNormal];
    [self.logoutButton addTarget: self action: @selector(logoutPressed) forControlEvents: UIControlEventTouchUpInside];
    
    [self.view addSubview: self.headerImageView];
    [self.view addSubview: self.headerLabel];
    [self.view addSubview: self.usernameLabel];
    [self.view addSubview: self.infoLabel];
    [self.view addSubview: self.stuff];
    [self.view addSubview: self.logoutButton];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)logoutPressed {
    [self dismissViewControllerAnimated: YES completion: nil];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 40)];
    view.backgroundColor = [UIColor whiteColor];
    if (section == 0)
        [view addSubview:self.newsLabel];
    else
        [view addSubview:self.facultiesLabel];
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSUInteger index = indexPath.section;
    if (index == 0) {
        NewsViewController *controller = [[NewsViewController alloc] initWithUsername: self.username];
        [self presentViewController: controller animated: YES completion: nil];
    } else if (index == 1) {
        FacultiesViewController *controller = [[FacultiesViewController alloc] initWithUsername: self.username];
        [self presentViewController: controller animated: YES completion: nil];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle: UITableViewCellStyleDefault reuseIdentifier: @"cell"];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    NSUInteger index = indexPath.section;
    if (index == 0) {
        cell.textLabel.text = @"Новости о нашем университете";
    } else if (index == 1) {
        cell.textLabel.text = @"Информация о наших факультетах";
    }
    
    return cell;
}



@end
