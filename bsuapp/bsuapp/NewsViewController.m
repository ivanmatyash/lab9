//
//  NewsViewController.m
//  bsuapp
//
//  Created by Admin on 26.05.15.
//  Copyright (c) 2015 dream tp team. All rights reserved.
//

#import "NewsViewController.h"

@interface NewsViewController ()

@property (strong, nonatomic) UIImageView *headerImageView;
@property (strong, nonatomic) UILabel *headerLabel;
@property (strong, nonatomic) UILabel *usernameLabel;
@property (strong, nonatomic) UIImageView *bsuImageView;
@property (strong, nonatomic) UILabel *newsLabel;
@property (strong, nonatomic) NSMutableArray *newsLabels;
@property (strong, nonatomic) UIButton *backButton;
@property (strong, nonatomic) NSString *username;

@end

@implementation NewsViewController

-(id)initWithUsername: (NSString *) username
{
    self = [super init];
    self.username = username;
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)initUI
{
    //__weak NewsViewController *weakSelf = self;
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.headerImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"header.png"]];
    self.headerImageView.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y + 20, self.view.frame.size.width, 70);
    
    self.headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(50, 25, 200, 60)];
    self.headerLabel.text = @"БЕЛОРУССКИЙ\nГОСУДАРСТВЕННЫЙ\nУНИВЕРСИТЕТ";
    self.headerLabel.font = [UIFont fontWithName:@"Helvetica" size:16];
    self.headerLabel.textColor = [UIColor whiteColor];
    self.headerLabel.numberOfLines = 3;
    
    self.usernameLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width - 130, 32, 80, 44)];
    self.usernameLabel.text = self.username;
    self.usernameLabel.textAlignment = NSTextAlignmentCenter;
    
    self.usernameLabel.font = [UIFont fontWithName:@"Helvetica" size:16];
    self.usernameLabel.textColor = [UIColor whiteColor];

    self.bsuImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo.jpg"]];
    self.bsuImageView.frame = CGRectMake(10, 95, 70, 110);
    
    self.newsLabel = [[UILabel alloc] initWithFrame:CGRectMake(140, 100, 100, 100)];
    self.newsLabel.text = @"Новости";
    self.newsLabel.font = [UIFont fontWithName:@"Helvetica Bold" size:21];
    self.newsLabel.textColor = [UIColor colorWithRed:10 / 255.f green:52 / 255.f blue:112 / 255.f alpha:1];
    
    self.newsLabels = [[NSMutableArray alloc] init];
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"bsuNews" ofType:@"plist"];
    NSArray *newsArray = [[NSArray alloc] initWithContentsOfFile:path];

    for (int i = 0; i < 3; ++i)
    {
        UITextView *bsuNewsTextView = [[UITextView alloc] initWithFrame:CGRectMake(self.view.frame.origin.x + 10, 210 + 150 * i, self.view.frame.size.width - 20, 140)];
        bsuNewsTextView.layer.borderWidth = 2;
        bsuNewsTextView.layer.borderColor = [UIColor colorWithRed:10 / 255.f green:52 / 255.f blue:112 / 255.f alpha:0.5].CGColor;
        
        bsuNewsTextView.text = newsArray[i];
        bsuNewsTextView.font = [UIFont fontWithName:@"Helvetica" size:17];
        bsuNewsTextView.editable = NO;
        [self.newsLabels addObject: bsuNewsTextView];
    }
    
    UIImage *backImage = [UIImage imageNamed:@"arrow.png"];
    self.backButton = [[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width - 35, 43, 25, 27)];
    [self.backButton setImage:backImage forState:UIControlStateNormal];
    [self.backButton addTarget:self action:@selector(backButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    
    for (UILabel *newsLabel in self.newsLabels)
    {
        [self.view addSubview:newsLabel];
    }
    [self.view addSubview: self.headerImageView];
    [self.view addSubview: self.headerLabel];
    [self.view addSubview: self.usernameLabel];
    [self.view addSubview:self.bsuImageView];
    [self.view addSubview:self.newsLabel];
    [self.view addSubview:self.backButton];
}

- (void) backButtonPressed
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
