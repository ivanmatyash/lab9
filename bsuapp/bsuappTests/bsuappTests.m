//
//  bsuappTests.m
//  bsuappTests
//
//  Created by Admin on 25.05.15.
//  Copyright (c) 2015 dream tp team. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>

#import "User.h"

@interface bsuappTests : XCTestCase

@end

@implementation bsuappTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample {
    // This is an example of a functional test case.
    XCTAssert(YES, @"Pass");
}

- (void)testUserEmptyField {
    User *user = [[User alloc] initUnbound];
    user.name = @"";
    user.login = @"a";
    user.password = @"b";
    
    XCTAssert(![user isValid], @"User with empty fields must not be valid");
}

- (void)testUserCorrect {
    User *user = [[User alloc] initUnbound];
    user.name = @"john";
    user.login = @"a";
    user.password = @"b";
    
    XCTAssert([user isValid], @"User is valid");
}

- (void)testUserNameTooLong {
    User *user = [[User alloc] initUnbound];
    user.name = @"asdsadfgagfshtfh";
    user.login = @"a";
    user.password = @"b";
    
    XCTAssert(![user isValid], @"User is invalid because his name is too long");
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

@end
